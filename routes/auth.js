const express = require("express");
const bcrypt = require("bcrypt");
const { body, validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
require("dotenv").config();

const router = express.Router();

module.exports = (params) => {
  const { userService } = params;
  //   Register a user
  router.post(
    "/register",
    body("email").isEmail(),
    body("password").isLength({ min: 6 }),
    async (req, res, next) => {
      const errors = validationResult(req);

      try {
        if (!errors.isEmpty() && errors.errors[0].param === "email") {
          return res
            .status(400)
            .send("Invalid email address. Please try again.");
        }
        if (!errors.isEmpty() && errors.errors[0].param === "password") {
          return res
            .status(400)
            .send("Password must be longer than 6 characters.");
        }

        const hashedPassword = await bcrypt.hash(req.body.password, 10);
        const userObj = { email: req.body.email, password: hashedPassword };
        const newUser = await userService.createUser(userObj);

        if (!newUser) {
          return res.status(400).send("User with this email already exists");
        }
      } catch (err) {
        next(err);
      }
    }
  );

  router.post("/login", async (req, res, next) => {
    if (!req.body.email || !req.body.password) {
      res.status(400).send("Please input correct fields");
    }
    const user = await userService.findUser(req.body.email);
    if (!user) {
      return res.status(400).send("Cannot find the user");
    }
    try {
      if (await bcrypt.compare(req.body.password, user.password)) {
        const accessToken = jwt.sign(user, process.env.ACCESS_TOKEN);
        res.json({ user: { email: user.email }, token: accessToken });
      } else {
        res.status(400).send("Access denied");
      }
    } catch {
      next(err);
    }
  });

  return router;
};
