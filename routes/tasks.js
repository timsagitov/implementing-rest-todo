const express = require("express");

const jwt = require("jsonwebtoken");
require("dotenv").config();

const authenticateToken = (req, res, next) => {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];
  if (token == null) return res.sendStatus(401);

  jwt.verify(token, process.env.ACCESS_TOKEN, (err, user) => {
    if (err) return res.sendStatus(403);
    req.user = user;
    next();
  });
};

const router = express.Router();

module.exports = (params) => {
  const { tasksService } = params;
  // CREATES NEW TASK
  router.post("/", authenticateToken, async (req, res) => {
    if (!req.body.title || !req.body.description)
      return res.status(400).send("Bad request");
    try {
      if (await tasksService.findTask(req.user.email, req.body.title))
        return res.status(400).send("The task already exists");

      await tasksService.addNewTask(
        req.user.email,
        req.body.title,
        req.body.description
      );
    } catch {
      return res.sendStatus(505);
    }
  });

  // GET LISTS OF TASKS
  router.get("/", authenticateToken, async (req, res) => {
    const tasks = await tasksService.getTasksOfUser(req.user.email);
    return res.json(tasks);
  });

  // MARK TASK AS DONE
  router.post("/done", authenticateToken, async (req, res) => {
    if (!req.body.title) return res.status(400).send("Bad Request");
    if (!tasksService.findTask(req.user.email, req.body.title)) {
      return res.status("404").send("Task not found");
    }
    try {
      await tasksService.markTaskDone(req.user.email, req.body.title);
    } catch {
      res.sendStatus(505);
    }
  });

  // GET COMPLETED TASKS
  router.get("/done", authenticateToken, async (req, res) => {
    const tasks = await tasksService.getCompletedTasks(req.user.email);
    return res.json(tasks);
  });

  // UPDATE A TASK
  router.put("/:taskTitle", authenticateToken, async (req, res) => {
    if (!req.body.title || !req.body.description)
      return res.status(400).send("Bad request");
    try {
      if (
        !(await tasksService.findTask(req.user.email, req.params.taskTitle))
      ) {
        return res.status(400).send("Task not found");
      }

      await tasksService.updateTask(
        req.user.email,
        req.params.taskTitle,
        req.body.title,
        req.body.description
      );
    } catch {
      return res.sendStatus(505);
    }
  });

  // DELETE A TASK
  router.delete("/", authenticateToken, async (req, res) => {
    if (!req.body.title) return res.status(400).send("Bad request");
    try {
      const successfullDeletion = await tasksService.deleteTask(
        email,
        taskTitle
      );
      if (successfullDeletion) return res.status(204);
    } catch {
      return res.sendStatus(505);
    }
  });

  return router;
};
