const fs = require("fs");
const util = require("util");

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

class UserService {
  /**
   * Constructor
   * @param {*} datafile Path to a JSOn file that contains the user data
   */
  constructor(datafile) {
    this.datafile = datafile;
  }

  async createUser(user) {
    const data = await this.getData();
    for (const existUser of data) {
      if (existUser.email === user.email) {
        return null;
      }
    }
    data.push(user);
    return writeFile(this.datafile, JSON.stringify(data));
  }

  async findUser(email) {
    const data = await this.getData();
    return data.find((user) => user.email === email);
  }

  /**
   * Fetches book data from the JSON file provided to the constructor
   */
  async getData() {
    const data = await readFile(this.datafile, "utf8");
    if (!data) return [];
    return JSON.parse(data);
  }
}

module.exports = UserService;
