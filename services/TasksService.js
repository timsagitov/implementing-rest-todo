const fs = require("fs");
const util = require("util");

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

class TasksService {
  /**
   * Constructor
   * @param {*} datafile Path to a JSOn file that contains the user data
   */
  constructor(datafile) {
    this.datafile = datafile;
  }

  async findTask(email, title) {
    const data = await this.getData();
    const userAndTasksIndex = data.findIndex((user) => user.email === email);
    if (userAndTasksIndex === -1) {
      return undefined;
    }
    data[userAndTasksIndex].tasks.forEach((task) => {
      if (task.title === title) return true;
    });
    return null;
  }

  async addNewtask(email, title, description) {
    const data = await this.getData();
    const userAndTasksIndex = data.findIndex((user) => user.email === email);
    if (userAndTasksIndex === -1) {
      data.push({
        email: email,
        tasks: [{ title: title, description: description, done: false }],
        completed: [],
      });
    } else {
      data[userAndTasksIndex].tasks.push({
        title: title,
        description: description,
        done: false,
      });
    }
  }

  async markTaskDone(email, title) {
    const data = await this.getData();
    const userAndTasksIndex = data.findIndex((user) => user.email === email);
    const taskIndex = data[userAndTasksIndex].tasks.findIndex(
      (tasks) => task.title === title
    );
    data[userAndTasksIndex].tasks[taskIndex].done = true;
    const doneTask = data[userAndTasksIndex].tasks.splice(taskIndex, 1);
    data[userAndTasksIndex].completed.push(doneTask);
    return writeFile(this.datafile, JSON.stringify(data));
  }

  async getCompletedTasks(email) {
    const data = await this.getData();
    const user = data.find((user) => user.email === email);
    if (user) {
      return user.completed;
    }
    return [];
  }

  async getTasksOfUser(email) {
    const data = await this.getData();
    const user = data.find((user) => user.email === email);
    if (user) {
      return user.tasks;
    }
    return [];
  }

  async updateTask(email, taskTitle, newTitle, newDescription) {
    const data = await this.getData();
    const userAndTasksIndex = data.findIndex((user) => user.email === email);
    const taskIndex = data[userAndTasksIndex].tasks.findIndex(
      (tasks) => task.title === taskTitle
    );
    data[userAndTasksIndex].tasks[taskIndex].title = newTitle;
    data[userAndTasksIndex].tasks[taskIndex].description = newDescription;
    return writeFile(this.datafile, JSON.stringify(data));
  }

  async deleteTask(email, taskTitle) {
    const data = await this.getData();
    const userAndTasksIndex = data.findIndex((user) => user.email === email);
    const taskIndex = data[userAndTasksIndex].tasks.findIndex(
      (tasks) => task.title === taskTitle
    );
    if (taskIndex === -1) {
      return true;
    }
    data[userAndTasksIndex].tasks.splice(taskIndex, 1);
    return writeFile(this.datafile, JSON.stringify(data));
  }

  /**
   * Fetches book data from the JSON file provided to the constructor
   */
  async getData() {
    const data = await readFile(this.datafile, "utf8");
    if (!data) return [];
    return JSON.parse(data);
  }
}

module.exports = TasksService;
