const express = require("express");

const authRoutes = require("./routes/auth");
const tasksRoutes = require("./routes/tasks");
const UserService = require("./services/UserService");
const TasksService = require("./services/TasksService");

const app = express();
const PORT = 3000;
const userService = new UserService("./db/users.json");
const tasksService = new TasksService("./db/tasks.json");

app.use(express.json());

app.use("/auth", authRoutes({ userService }));
app.use("/tasks", tasksRoutes({ tasksService }));

app.listen(3000, () => {
  console.log(`Server has successfully started at ---PORT:${PORT}---`);
});
